FROM node:6
ADD ./package.json /wlsjs/package.json
WORKDIR /wlsjs
RUN npm install
ADD . /wlsjs
RUN npm test
