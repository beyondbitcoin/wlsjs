# Documentation

- [Install](#install)
- [Browser](#browser)
- [Config](#config)
- [JSON-RPC](#jsonrpc)
- [Database API](#api)
    - [Subscriptions](#subscriptions)
    - [Tags](#tags)
    - [Blocks and transactions](#blocks-and-transactions)
    - [Globals](#globals)
    - [Keys](#keys)
    - [Accounts](#accounts)
    - [Market](#market)
    - [Authority / validation](#authority--validation)
    - [Votes](#votes)
    - [Content](#content)
    - [Witnesses](#witnesses)
- [Login API](#login)
- [Follow API](#follow-api)
- [Broadcast API](#broadcast-api)
- [Broadcast](#broadcast)
- [Auth](#auth)
- [Formatter](#formatter)

# Install
```
$ npm install @whaleshares/wlsjs --save
```

# Browser 
```html 
<script src="./wlsjs.min.js"></script>
<script>
wlsjs.api.getAccounts(['officialfuzzy', 'powerpics'], function(err, response){
    console.log(err, response);
});
</script>
```

## Config
Default config should work with wlsjs.
### set
```
wlsjs.config.set('address_prefix','WLS');
```
### get
```
wlsjs.config.get('chain_id');
```

## JSON-RPC
Here is how to activate JSON-RPC transport:
```js
wlsjs.api.setOptions({ url: 'wss://whaleshares.io/ws' });
```

# API

## Subscriptions

### Set Subscribe Callback
```
wlsjs.api.setSubscribeCallback(callback, clearFilter, function(err, result) {
  console.log(err, result);
});
```
### Set Pending Transaction Callback
```
wlsjs.api.setPendingTransactionCallback(cb, function(err, result) {
  console.log(err, result);
});
```
### Set Block Applied Callback
```
wlsjs.api.setBlockAppliedCallback(cb, function(err, result) {
  console.log(err, result);
});
```
### Cancel All Subscriptions
```
wlsjs.api.cancelAllSubscriptions(function(err, result) {
  console.log(err, result);
});
```

## Tags

### Get Tags Used By Author
```
wlsjs.api.getTagsUsedByAuthor(author, function(err, result) {
  console.log(err, result);
});
```
### Get Trending Tags
```
wlsjs.api.getTrendingTags(afterTag, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Trending
```
wlsjs.api.getDiscussionsByTrending(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Created
```
wlsjs.api.getDiscussionsByCreated(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Active
```
wlsjs.api.getDiscussionsByActive(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Cashout
```
wlsjs.api.getDiscussionsByCashout(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Payout
```
wlsjs.api.getDiscussionsByPayout(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Votes
```
wlsjs.api.getDiscussionsByVotes(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Children
```
wlsjs.api.getDiscussionsByChildren(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Hot
```
wlsjs.api.getDiscussionsByHot(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Feed
```
wlsjs.api.getDiscussionsByFeed(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Blog
```
wlsjs.api.getDiscussionsByBlog(query, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Comments
```
wlsjs.api.getDiscussionsByComments(query, function(err, result) {
  console.log(err, result);
});
```

## Blocks and transactions

### Get Block Header
```
wlsjs.api.getBlockHeader(blockNum, function(err, result) {
  console.log(err, result);
});
```
### Get Block
```
wlsjs.api.getBlock(blockNum, function(err, result) {
  console.log(err, result);
});
```
### Get Ops In Block
```
wlsjs.api.getOpsInBlock(blockNum, onlyVirtual, function(err, result) {
  console.log(err, result);
});
```
### Get State
```
wlsjs.api.getState(path, function(err, result) {
  console.log(err, result);
});
```
### Get Trending Categories
```
wlsjs.api.getTrendingCategories(after, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Best Categories
```
wlsjs.api.getBestCategories(after, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Active Categories
```
wlsjs.api.getActiveCategories(after, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Recent Categories
```
wlsjs.api.getRecentCategories(after, limit, function(err, result) {
  console.log(err, result);
});
```

## Globals

### Get Config
```
wlsjs.api.getConfig(function(err, result) {
  console.log(err, result);
});
```
### Get Dynamic Global Properties
```
wlsjs.api.getDynamicGlobalProperties(function(err, result) {
  console.log(err, result);
});
```
### Get Chain Properties
```
wlsjs.api.getChainProperties(function(err, result) {
  console.log(err, result);
});
```
### Get Hardfork Version
```
wlsjs.api.getHardforkVersion(function(err, result) {
  console.log(err, result);
});
```
### Get Next Scheduled Hardfork
```
wlsjs.api.getNextScheduledHardfork(function(err, result) {
  console.log(err, result);
});
```
### Get Reward Fund
```
wlsjs.api.getRewardFund(name, function(err, result) {
  console.log(err, result);
});
```
### Get Version
```
wlsjs.api.getVersion(function(err, result) {
  console.log(result);
});
```

## Keys

### Get Key References
```
wlsjs.api.getKeyReferences(key, function(err, result) {
  console.log(err, result);
});
```

## Accounts

### Get Accounts
```
wlsjs.api.getAccounts(names, function(err, result) {
  console.log(err, result);
});
```
### Get Account References
```
wlsjs.api.getAccountReferences(accountId, function(err, result) {
  console.log(err, result);
});
```
### Lookup Account Names
```
wlsjs.api.lookupAccountNames(accountNames, function(err, result) {
  console.log(err, result);
});
```
### Lookup Accounts
```
wlsjs.api.lookupAccounts(lowerBoundName, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Account Count
```
wlsjs.api.getAccountCount(function(err, result) {
  console.log(err, result);
});
```
### Get Account History
```
wlsjs.api.getAccountHistory(account, from, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Owner History
```
wlsjs.api.getOwnerHistory(account, function(err, result) {
  console.log(err, result);
});
```
### Get Withdraw Routes
```
wlsjs.api.getWithdrawRoutes(account, withdrawRouteType, function(err, result) {
  console.log(result);
});
```
### Get Account Bandwidth
```
wlsjs.api.getAccountBandwidth(account bandwidthType, function(err, result) {
  console.log(result);
});
```

## Authority / validation

### Get Transaction Hex
```
wlsjs.api.getTransactionHex(trx, function(err, result) {
  console.log(err, result);
});
```
### Get Transaction
```
wlsjs.api.getTransaction(trxId, function(err, result) {
  console.log(err, result);
});
```
### Get Required Signatures
```
wlsjs.api.getRequiredSignatures(trx, availableKeys, function(err, result) {
  console.log(err, result);
});
```
### Get Potential Signatures
```
wlsjs.api.getPotentialSignatures(trx, function(err, result) {
  console.log(err, result);
});
```
### Verify Authority
```
wlsjs.api.verifyAuthority(trx, function(err, result) {
  console.log(err, result);
});
```
### Verify Account Authority
```
wlsjs.api.verifyAccountAuthority(nameOrId, signers, function(err, result) {
  console.log(err, result);
});
```

## Votes

### Get Active Votes
```
wlsjs.api.getActiveVotes(author, permlink, function(err, result) {
  console.log(err, result);
});
```
### Get Account Votes
```
wlsjs.api.getAccountVotes(voter, function(err, result) {
  console.log(err, result);
});
```

## Content

### Get Post Discussions By Payout
```
wlsjs.api.getPostDiscussionsByPayout(query, function(err, result) {
  console.log(err, result);
});
```

### Get Comments Discussions By Payout
```
wlsjs.api.getCommentDiscussionsByPayout(query, function(err, result) {
  console.log(err, result);
});
```

### Get Content
```
wlsjs.api.getContent(author, permlink, function(err, result) {
  console.log(err, result);
});
```
### Get Content Replies
```
wlsjs.api.getContentReplies(author, permlink, function(err, result) {
  console.log(err, result);
});
```
### Get Discussions By Author Before Date
```
wlsjs.api.getDiscussionsByAuthorBeforeDate(author, startPermlink, beforeDate, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Replies By Last Update
```
wlsjs.api.getRepliesByLastUpdate(startAuthor, startPermlink, limit, function(err, result) {
  console.log(err, result);
});
```

## Witnesses


### Get Witnesses
```
wlsjs.api.getWitnesses(witnessIds, function(err, result) {
  console.log(err, result);
});
```
### Get Witness By Account
```
wlsjs.api.getWitnessByAccount(accountName, function(err, result) {
  console.log(err, result);
});
```
### Get Witnesses By Vote
```
wlsjs.api.getWitnessesByVote(from, limit, function(err, result) {
  console.log(err, result);
});
```
### Lookup Witness Accounts
```
wlsjs.api.lookupWitnessAccounts(lowerBoundName, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Witness Count
```
wlsjs.api.getWitnessCount(function(err, result) {
  console.log(err, result);
});
```
### Get Active Witnesses
```
wlsjs.api.getActiveWitnesses(function(err, result) {
  console.log(err, result);
});
```
### Get Miner Queue
```
wlsjs.api.getMinerQueue(function(err, result) {
  console.log(err, result);
});
```

## Login API

### Login

/!\ It's **not safe** to use this method with your username and password. This method always return `true` and is only used in intern with empty values to enable broadcast.

```
wlsjs.api.login('', '', function(err, result) {
  console.log(err, result);
});
```

### Get Api By Name
```
wlsjs.api.getApiByName(apiName, function(err, result) {
  console.log(err, result);
});
```

## Follow API

### Get Followers
```
wlsjs.api.getFollowers(following, startFollower, followType, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Following
```
wlsjs.api.getFollowing(follower, startFollowing, followType, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Follow Count
```
wlsjs.api.getFollowCount(account, function(err, result) {
  console.log(err, result);
});
```
### Get Feed Entries
```
wlsjs.api.getFeedEntries(account, entryId, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Feed
```
wlsjs.api.getFeed(account, entryId, limit, function(err, result) {
  console.log(err, result);
})
```
### Get Blog Entries
```
wlsjs.api.getBlogEntries(account, entryId, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Blog
```
wlsjs.api.getBlog(account, entryId, limit, function(err, result) {
  console.log(err, result);
});
```
### Get Reblogged By
```
wlsjs.api.getRebloggedBy(author, permlink, function(err, result) {
  console.log(err, result);
});
```
### Get Blog Authorsz
```
wlsjs.api.getBlogAuthors(blogAccount, function(err, result) {
  console.log(err, result);
});
```

## Broadcast API

### Broadcast Transaction Synchronous
```
wlsjs.api.broadcastTransactionSynchronous(trx, function(err, result) {
  console.log(err, result);
});
```
### Broadcast Block
```
wlsjs.api.broadcastBlock(b, function(err, result) {
  console.log(err, result);
});
```

# Broadcast

### Account Buying
```
wlsjs.broadcast.accountBuying(wif, account, account_buy, price, function(err, result) {
  console.log(err, result);
});
```
### Account Create
```
wlsjs.broadcast.accountCreate(wif, fee, creator, newAccountName, owner, active, posting, memoKey, jsonMetadata, function(err, result) {
  console.log(err, result);
});
```
### Account For Sale
```
wlsjs.broadcast.accountForsale(wif, account, for_sale, to, function(err, result) {
  console.log(err, result);
});
```
### Account Update
```
wlsjs.broadcast.accountUpdate(wif, account, owner, active, posting, memoKey, jsonMetadata, function(err, result) {
  console.log(err, result);
});
```
### Account Witness Proxy
```
wlsjs.broadcast.accountWitnessProxy(wif, account, proxy, function(err, result) {
  console.log(err, result);
});
```
### Account Witness Vote
```
wlsjs.broadcast.accountWitnessVote(wif, account, witness, approve, function(err, result) {
  console.log(err, result);
});
```
### Claim Reward Balance
```
wlsjs.broadcast.claimRewardBalance(wif, account, reward_steem, reward_vests, function(err, result) {
  console.log(err, result);
});
```
### Comment
```
wlsjs.broadcast.comment(wif, parentAuthor, parentPermlink, author, permlink, title, body, jsonMetadata, function(err, result) {
  console.log(err, result);
});
```
### Comment Options
```
wlsjs.broadcast.commentOptions(wif, author, permlink, maxAcceptedPayout, percentSteemDollars, allowVotes, allowCurationRewards, extensions, function(err, result) {
  console.log(err, result);
});
```
### Comment Reward
```
wlsjs.broadcast.commentReward(wif, author, permlink, sbdPayout, vestingPayout, function(err, result) {
  console.log(err, result);
});
```
### Custom
```
wlsjs.broadcast.custom(wif, requiredAuths, id, data, function(err, result) {
  console.log(err, result);
});
```
### Custom Binary
```
wlsjs.broadcast.customBinary(wif, id, data, function(err, result) {
  console.log(err, result);
});
```
### Custom Json
```
wlsjs.broadcast.customJson(wif, requiredAuths, requiredPostingAuths, id, json, function(err, result) {
  console.log(err, result);
});
```
### Delete Comment
```
wlsjs.broadcast.deleteComment(wif, author, permlink, function(err, result) {
  console.log(err, result);
});
```
### Fill Vesting Withdraw
```
wlsjs.broadcast.fillVestingWithdraw(wif, fromAccount, toAccount, withdrawn, deposited, function(err, result) {
  console.log(err, result);
});
```
### Set Withdraw Vesting Route
```
wlsjs.broadcast.setWithdrawVestingRoute(wif, fromAccount, toAccount, percent, autoVest, function(err, result) {
  console.log(err, result);
});
```
### Transfer
```
wlsjs.broadcast.transfer(wif, from, to, amount, memo, function(err, result) {
  console.log(err, result);
});
```
### Transfer To Vesting
```
wlsjs.broadcast.transferToVesting(wif, from, to, amount, function(err, result) {
  console.log(err, result);
});
```
### Vote
```
wlsjs.broadcast.vote(wif, voter, author, permlink, weight, function(err, result) {
  console.log(err, result);
});
```
### Withdraw Vesting
```
wlsjs.broadcast.withdrawVesting(wif, account, vestingShares, function(err, result) {
  console.log(err, result);
});
```
### Witness Update
```
wlsjs.broadcast.witnessUpdate(wif, owner, url, blockSigningKey, props, fee, function(err, result) {
  console.log(err, result);
});
```

### Multisig
You can use multisignature to broadcast an operation.
```
wlsjs.broadcast.send({
  extensions: [],
  operations: [
    ['vote', {
      voter: 'kennybll',
      author: 'whaleshares',
      permlink: 'whaleshares-pre-launch-open-beta-is-here',
      weight: 100
    }]
  ]}, [privPostingWif1, privPostingWif2], (err, result) => {
  console.log(err, result);
});
```

# Auth

### Verify
```
wlsjs.auth.verify(name, password, auths);
```

### Generate Keys
```
wlsjs.auth.generateKeys(name, password, roles);
```

### Get Private Keys
```
wlsjs.auth.getPrivateKeys(name, password, roles);
```

### Is Wif
```
wlsjs.auth.isWif(privWif);
```

### To Wif
```
wlsjs.auth.toWif(name, password, role);
```

### Wif Is Valid
```
wlsjs.auth.wifIsValid(privWif, pubWif);
```

### Wif To Public
```
wlsjs.auth.wifToPublic(privWif);
```

### Sign Transaction
```
wlsjs.auth.signTransaction(trx, keys);
```

# Formatter

### Create Suggested Password
```
var password = wlsjs.formatter.createSuggestedPassword();
console.log(password);
// => 'GAz3GYFvvQvgm7t2fQmwMDuXEzDqTzn9'
```

### Comment Permlink
```
var parentAuthor = 'whaleshares';
var parentPermlink = 'whaleshares-pre-launch-open-beta-is-here';
var commentPermlink = wlsjs.formatter.commentPermlink(parentAuthor, parentPermlink);
console.log(commentPermlink);
// => 're-whaleshares-whaleshares-pre-launch-open-beta-is-here-20170621t080403765z'
```

# Utils

### Validate Username
```
var isValidUsername = wlsjs.utils.validateAccountName('test1234');
console.log(isValidUsername);
// => 'null'

var isValidUsername = wlsjs.utils.validateAccountName('a1');
console.log(isValidUsername);
// => 'Account name should be longer.'
```
