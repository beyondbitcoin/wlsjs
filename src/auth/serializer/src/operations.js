
// This file is merge updated from steemd's js_operation_serializer program.
/*

./js_operation_serializer |
sed 's/void/future_extensions/g'|
sed 's/steemit_protocol:://g'|
sed 's/14static_variantIJNS_12fixed_stringINSt3__14pairIyyEEEEEEE/string/g'|
sed 's/steemit_future_extensions/future_extensions/g'|
sed 's/steemit_protocol_//g' > tmp.coffee

*/
// coffee tmp.coffee # fix errors until you see: `ChainTypes is not defined`

// npm i -g decaffeinate
// decaffeinate tmp.coffee

// Merge tmp.js - See "Generated code follows" below

import types from "./types"
import SerializerImpl from "./serializer"

const {
    //id_type,
    //varint32, uint8, int64, fixed_array, object_id_type, vote_id, address,
    uint16, uint32, int16, uint64,
    string, string_binary, bytes, bool, array,
    // protocol_id_type,
    static_variant, map, set,
    public_key,
    time_point_sec,
    optional,
    asset,
} = types

const future_extensions = types.void
const hardfork_version_vote = types.void
const version = types.void

// Place-holder, their are dependencies on "operation" .. The final list of
// operations is not avialble until the very end of the generated code.
// See: operation.st_operations = ...
const operation = static_variant();
module.exports.operation = operation;

// For module.exports
const Serializer=function(operation_name, serilization_types_object){
    const s = new SerializerImpl(operation_name, serilization_types_object);
    return module.exports[operation_name] = s;
}

const beneficiaries = new Serializer("beneficiaries", {
  account: string,
  weight: uint16
});

const comment_payout_beneficiaries = new Serializer(0, {
  beneficiaries: set(beneficiaries)
});

// Custom-types after Generated code

// ##  Generated code follows
// -------------------------------
/*
When updating generated code (fix closing notation)
Replace:  var operation = static_variant([
with:     operation.st_operations = [

Delete (these are custom types instead):
let public_key = new Serializer(
    "public_key",
    {key_data: bytes(33)}
);

let asset = new Serializer(
    "asset",
    {amount: int64,
    symbol: uint64}
);

Replace: authority.prototype.account_authority_map
With: map((string), (uint16))
*/
let signed_transaction = new Serializer(
  "signed_transaction", {
    ref_block_num: uint16,
    ref_block_prefix: uint32,
    expiration: time_point_sec,
    operations: array(operation),
    extensions: set(future_extensions),
    signatures: array(bytes(65))
  }
);

let signed_block = new Serializer(
  "signed_block", {
    previous: bytes(20),
    timestamp: time_point_sec,
    witness: string,
    transaction_merkle_root: bytes(20),
    extensions: set(static_variant([
      future_extensions,
      version,
      hardfork_version_vote
    ])),
    witness_signature: bytes(65),
    transactions: array(signed_transaction)
  }
);

let block_header = new Serializer(
  "block_header", {
    previous: bytes(20),
    timestamp: time_point_sec,
    witness: string,
    transaction_merkle_root: bytes(20),
    extensions: set(static_variant([
      future_extensions,
      version,
      hardfork_version_vote
    ]))
  }
);

let signed_block_header = new Serializer(
  "signed_block_header", {
    previous: bytes(20),
    timestamp: time_point_sec,
    witness: string,
    transaction_merkle_root: bytes(20),
    extensions: set(static_variant([
      future_extensions,
      version,
      hardfork_version_vote
    ])),
    witness_signature: bytes(65)
  }
);

let vote = new Serializer(
  "vote", {
    voter: string,
    author: string,
    permlink: string,
    weight: int16
  }
);

let comment = new Serializer(
  "comment", {
    parent_author: string,
    parent_permlink: string,
    author: string,
    permlink: string,
    title: string,
    body: string,
    json_metadata: string
  }
);

let transfer = new Serializer(
  "transfer", {
    from: string,
    to: string,
    amount: asset,
    memo: string
  }
);

let transfer_to_vesting = new Serializer(
  "transfer_to_vesting", {
    from: string,
    to: string,
    amount: asset
  }
);

let withdraw_vesting = new Serializer(
  "withdraw_vesting", {
    account: string,
    vesting_shares: asset
  }
);

let price = new Serializer(
  "price", {
    base: asset,
    quote: asset
  }
);

var authority = new Serializer(
  "authority", {
    weight_threshold: uint32,
    account_auths: map((string), (uint16)),
    key_auths: map((public_key), (uint16))
  }
);

let account_create = new Serializer(
  "account_create", {
    fee: asset,
    creator: string,
    new_account_name: string,
    owner: authority,
    active: authority,
    posting: authority,
    memo_key: public_key,
    json_metadata: string
  }
);

let account_update = new Serializer(
  "account_update", {
    account: string,
    owner: optional(authority),
    active: optional(authority),
    posting: optional(authority),
    memo_key: public_key,
    json_metadata: string
  }
);

const account_action_create_pod = new Serializer(0, {
  fee: asset,
  join_fee: optional(asset),
  json_metadata: optional(string),
  allow_join: optional(bool)
});

const account_action_transfer_to_tip = new Serializer(1, {
  amount: asset
});

const htlc_sha256 = new Serializer(0, {
  htlc_hash: bytes(32)
});

const account_action_htlc_create = new Serializer(2, {
  fee: asset,
  reward: asset,
  to: string,
  amount: asset,
  preimage_hash: static_variant([
    htlc_sha256
  ]),
  preimage_size: uint16,
  expiration: uint32,
  memo: optional(string)
});

const account_action_htlc_update = new Serializer(3, {
  fee: asset,
  htlc_id: bytes(20),
  seconds: uint32,
  reward: asset,
  memo: optional(string)
});

const account_action_htlc_redeem = new Serializer(4, {
  fee: asset,
  htlc_id: bytes(20),
  preimage: bytes(),
  memo: optional(string)
});

let account_action = new Serializer(
  "account_action", {
    account: string,
    action: static_variant([
      account_action_create_pod,
      account_action_transfer_to_tip,
      account_action_htlc_create,
      account_action_htlc_update,
      account_action_htlc_redeem
    ])
  }
);

const social_action_comment_create = new Serializer(0, {
  permlink: string,
  parent_author: string,
  parent_permlink: string,
  pod: optional(string),
  max_accepted_payout: optional(asset),
  allow_replies: optional(bool),
  allow_votes: optional(bool),
  allow_curation_rewards: optional(bool),
  allow_friends: optional(bool),
  title: string,
  body: string,
  json_metadata: string
});

const social_action_comment_update = new Serializer(1, {
  permlink: string,
  title: optional(string),
  body: optional(string),
  json_metadata: optional(string)
});

const social_action_comment_delete = new Serializer(2, {
  permlink: string
});

const social_action_claim_vesting_reward = new Serializer(3, {
  amount: asset,
  to: optional(string),
  memo: optional(string)
});

const social_action_claim_vesting_reward_tip = new Serializer(4, {
  amount: asset
});

const social_action_user_tip = new Serializer(5, {
  amount: asset,
  to: optional(string),
  memo: optional(string)
});

const social_action_comment_tip = new Serializer(6, {
  author: string,
  permlink: string,
  amount: asset,
  memo: optional(string)
});

let social_action = new Serializer(
  "social_action", {
    account: string,
    action: static_variant([
      /** 0 */ social_action_comment_create,
      /** 1 */ social_action_comment_update,
      /** 2 */ social_action_comment_delete,
      /** 3 */ social_action_claim_vesting_reward,
      /** 4 */ social_action_claim_vesting_reward_tip,
      /** 5 */ social_action_user_tip,
      /** 6 */ social_action_comment_tip
    ])
  }
);

let chain_properties = new Serializer(
  "chain_properties", {
    account_creation_fee: asset,
    maximum_block_size: uint32
  }
);

let witness_update = new Serializer(
  "witness_update", {
    owner: string,
    url: string,
    block_signing_key: public_key,
    props: chain_properties,
    fee: asset
  }
);

let account_witness_vote = new Serializer(
  "account_witness_vote", {
    account: string,
    witness: string,
    approve: bool
  }
);

let account_witness_proxy = new Serializer(
  "account_witness_proxy", {
    account: string,
    proxy: string
  }
);

let custom = new Serializer(
  "custom", {
    required_auths: set(string),
    id: uint16,
    data: bytes()
  }
);

let delete_comment = new Serializer(
  "delete_comment", {
    author: string,
    permlink: string
  }
);

let custom_json = new Serializer(
  "custom_json", {
    required_auths: set(string),
    required_posting_auths: set(string),
    id: string,
    json: string
  }
);

let comment_options = new Serializer(
  "comment_options", {
    author: string,
    permlink: string,
    max_accepted_payout: asset,
    allow_votes: bool,
    allow_curation_rewards: bool,
    extensions: set(static_variant([
      comment_payout_beneficiaries
    ]))
  }
);

let set_withdraw_vesting_route = new Serializer(
  "set_withdraw_vesting_route", {
    from_account: string,
    to_account: string,
    percent: uint16,
    auto_vest: bool
  }
);

let custom_binary = new Serializer(
  "custom_binary", {
    required_owner_auths: set(string),
    required_active_auths: set(string),
    required_posting_auths: set(string),
    required_auths: array(authority),
    id: string,
    data: bytes()
  }
);

let claim_reward_balance = new Serializer(
  "claim_reward_balance", {
    account: string,
    reward_steem: asset,
    reward_vests: asset
  }
);

const friend_action_send_request = new Serializer(0, {
  memo: string
});

const friend_action_cancel_request = new Serializer(1, {
});

const friend_action_accept_request = new Serializer(2, {
});

const friend_action_reject_request = new Serializer(3, {
});

const friend_action_unfriend = new Serializer(4, {
});

let friend_action = new Serializer(
  "friend_action", {
    account: string,
    another: string,
    action: static_variant([
      friend_action_send_request,
      friend_action_cancel_request,
      friend_action_accept_request,
      friend_action_reject_request,
      friend_action_unfriend
    ])
  }
);

const pod_action_join_request = new Serializer(0, {
  join_fee: asset,
  fee: asset,
  memo: string
});

const pod_action_cancel_join_request = new Serializer(1, {});

const pod_action_accept_join_request = new Serializer(2, {
  account: string
});

const pod_action_reject_join_request = new Serializer(3, {
  account: string,
  memo: string
});

const pod_action_leave = new Serializer(4, {});

const pod_action_kick = new Serializer(5, {
  account: string,
  memo: string
});

const pod_action_update = new Serializer(6, {
  join_fee: optional(asset),
  json_metadata: optional(string),
  allow_join: optional(bool)
});

const pod_action = new Serializer(
  "pod_action", {
    account: string,
    pod: string,
    action: static_variant([
      pod_action_join_request,
      pod_action_cancel_join_request,
      pod_action_accept_join_request,
      pod_action_reject_join_request,
      pod_action_leave,
      pod_action_kick,
      pod_action_update
    ])
  }
);

const author_reward = new Serializer(
  "author_reward", {
    author: string,
    permlink: string,
    sbd_payout: asset,
    steem_payout: asset,
    vesting_payout: asset
  }
);

const curation_reward = new Serializer(
  "curation_reward", {
    curator: string,
    reward: asset,
    comment_author: string,
    comment_permlink: string
  }
);

const comment_reward = new Serializer(
  "comment_reward", {
    author: string,
    permlink: string,
    payout: asset
  }
);

const fill_vesting_withdraw = new Serializer(
  "fill_vesting_withdraw", {
    from_account: string,
    to_account: string,
    withdrawn: asset,
    deposited: asset
  }
);

const shutdown_witness = new Serializer(
  "shutdown_witness",
  { owner: string }
);

const hardfork = new Serializer(
  "hardfork",
  { hardfork_id: uint32 }
);

const comment_payout_update = new Serializer(
  "comment_payout_update", {
    author: string,
    permlink: string
  }
);

const comment_benefactor_reward = new Serializer(
  "comment_benefactor_reward", {
    benefactor: string,
    author: string,
    permlink: string,
    reward: asset
  }
);

const producer_reward = new Serializer(
  "producer_reward", {
    producer: string,
    vesting_shares: asset
  }
);

const devfund = new Serializer(
  "devfund", {
    account: string,
    reward: asset
  }
);

const pod_escrow_transfer_virtual_action = new Serializer(0, {
    account: string,
    pod: string,
    amount: asset
  }
);

const pod_escrow_release_virtual_action = new Serializer(1, {
    account: string,
    pod: string,
    amount: asset
  }
);

const pod_virtual = new Serializer(
  "pod_virtual", {
    action: static_variant([
      pod_escrow_transfer_virtual_action,
      pod_escrow_release_virtual_action
    ])
  }
);

const htlc_redeemed_virtual_action = new Serializer(0, {
  htlc_id: bytes(20),
  from: string,
  to: string,
  redeemer: string,
  amount: asset
});

const htlc_refund_virtual_action = new Serializer(1, {
  htlc_id: bytes(20),
  to: string
});

const htlc_virtual = new Serializer(
  "htlc_virtual", {
    action: static_variant([
      htlc_redeemed_virtual_action,
      htlc_refund_virtual_action
    ])
  }
);

operation.st_operations = [
  /*  0 */ vote,
  /*  1 */ comment,
  /*  2 */ transfer,
  /*  3 */ transfer_to_vesting,
  /*  4 */ withdraw_vesting,
  /*  5 */ account_create,
  /*  6 */ account_update,
  /*  7 */ account_action,
  /*  8 */ social_action,
  /*  9 */ witness_update,
  /* 10 */ account_witness_vote,
  /* 11 */ account_witness_proxy,
  /* 12 */ custom,
  /* 13 */ delete_comment,
  /* 14 */ custom_json,
  /* 15 */ comment_options,
  /* 16 */ set_withdraw_vesting_route,
  /* 17 */ custom_binary,
  /* 18 */ claim_reward_balance,
  /* 19 */ friend_action,
  /* 20 */ pod_action,
  /* 21 */ author_reward,
  /* 22 */ curation_reward,
  /* 23 */ comment_reward,
  /* 24 */ fill_vesting_withdraw,
  /* 25 */ shutdown_witness,
  /* 26 */ hardfork,
  /* 27 */ comment_payout_update,
  /* 28 */ comment_benefactor_reward,
  /* 29 */ producer_reward,
  /* 30 */ devfund,
  /* 31 */ pod_virtual,
  /* 32 */ htlc_virtual
];

let transaction = new Serializer(
  "transaction", {
    ref_block_num: uint16,
    ref_block_prefix: uint32,
    expiration: time_point_sec,
    operations: array(operation),
    extensions: set(future_extensions)
  }
);

//# -------------------------------
//#  Generated code end  S T O P
//# -------------------------------

// Custom Types (do not over-write)

const encrypted_memo = new Serializer(
  "encrypted_memo",
  {
    from: public_key,
    to: public_key,
    nonce: uint64,
    check: uint32,
    encrypted: string_binary
  }
);
/*

// Make sure all tests pass

npm test

*/
